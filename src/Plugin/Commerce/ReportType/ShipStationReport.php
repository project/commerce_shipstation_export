<?php

namespace Drupal\commerce_shipstation_export\Plugin\Commerce\ReportType;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_reports\Plugin\Commerce\ReportType\ReportTypeBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides the Order Items Report.
 *
 * @CommerceReportType(
 *   id = "shipstation_report",
 *   label = @Translation("Shipstation Report"),
 *   description = @Translation("Export for importing into Shipstation.")
 * )
 */
class ShipStationReport extends ReportTypeBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];
    $fields['order_item_type_id'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel($this->t("Order item type"))
      ->setDescription($this->t('The order item type.'))
      ->setSetting('target_type', 'commerce_order_item_type')
      ->setRequired(TRUE);
    $fields['order_item_id'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel($this->t('Order item'))
      ->setDescription($this->t('The parent order item.'))
      ->setSetting('target_type', 'commerce_order_item')
      ->setRequired(TRUE);
    $fields['title'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Title'))
      ->setDescription($this->t('The purchased item title.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 512,
      ]);
    $fields['quantity'] = BundleFieldDefinition::create('decimal')
      ->setLabel($this->t('Quantity'))
      ->setDescription($this->t('The number of purchased units.'))
      ->setRequired(TRUE)
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 0)
      ->setDisplayConfigurable('view', TRUE);
    $fields['unit_price'] = BundleFieldDefinition::create('commerce_price')
      ->setLabel($this->t('Unit price'))
      ->setDescription($this->t('The unit price for the purchased item'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['total_price'] = BundleFieldDefinition::create('commerce_price')
      ->setLabel($this->t('Total price'))
      ->setDescription($this->t('The total price for the purchased item'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['adjusted_unit_price'] = BundleFieldDefinition::create('commerce_price')
      ->setLabel($this->t('Adjusted unit price'))
      ->setDescription($this->t('The adjusted unit price for the purchased item'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['adjusted_total_price'] = BundleFieldDefinition::create('commerce_price')
      ->setLabel($this->t('Adjusted total price'))
      ->setDescription($this->t('The adjusted total price for the purchased item'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['order_type_id'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel($this->t('Order type'))
      ->setDescription($this->t('The order type.'))
      ->setSetting('target_type', 'commerce_order_type')
      ->setReadOnly(TRUE);
    $fields['amount'] = BundleFieldDefinition::create('commerce_price')
      ->setLabel($this->t('Total Amount'))
      ->setDescription($this->t('The total amount of the order'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['billing_address'] = BundleFieldDefinition::create('address')
      ->setLabel($this->t('Address'))
      ->setDescription($this->t('Order billing address.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['shipping_address'] = BundleFieldDefinition::create('address')
      ->setLabel($this->t('Shipping Address'))
      ->setDescription($this->t('Order shipping address.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['mail'] = BundleFieldDefinition::create('email')
      ->setLabel($this->t('Contact email'))
      ->setDescription($this->t('The email address associated with the order.'))
      ->setCardinality(1)
      ->setSetting('max_length', 255)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  protected function doBuildReportTableHeaders() {
    return [
      'formatted_date' => $this->t('Date'),
      'order_id_count' => $this->t('# Orders'),
      'quantity_sum' => $this->t('# Sold'),
      'adjusted_unit_price_sum' => $this->t('Total revenue'),
      'adjusted_total_price_sum' => $this->t('Average revenue'),
      'unit_price_currency_code' => $this->t('Currency'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function doBuildReportTableRow(array $result) {
    $currency_code = $result['unit_price_currency_code'];
    $row = [
      $result['formatted_date'],
      $result['order_id_count'],
      $result['quantity_sum'],
      [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{{price|commerce_price_format}}',
          '#context' => [
            'price' => new Price($result['adjusted_unit_price_sum'], $currency_code),
          ],
        ],
      ],
      [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{{price|commerce_price_format}}',
          '#context' => [
            'price' => new Price($result['adjusted_total_price_sum'], $currency_code),
          ],
        ],
      ],
      $currency_code,
    ];
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function generateReports(OrderInterface $order) {

    $billing_profile = $order->getBillingProfile();
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $billing_profile->get('address')->first();

    $shipping_values = [
      'shipping_address' => 'N/A',
    ];

    if ($order->hasField('shipments') || !$order->get('shipments')->isEmpty()) {
      $shipments = $order->get('shipments')->getValue();
      if ($shipments) {
        /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
        $shipment = \Drupal::entityTypeManager()->getStorage('commerce_shipment')->load($shipments[0]['target_id']);
        /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $ship */
        $shipping_profile = $shipment->getShippingProfile()->get('address')->first();
        $shipping_values = [
          'shipping_address' => $shipping_profile->toArray(),
        ];
      }
    }

    $order_values = [
      'order_type_id' => $order->bundle(),
      'amount' => $order->getTotalPrice(),
      'mail' => $order->getEmail(),
      'billing_address' => $address->toArray(),
    ];

    foreach ($order->getItems() as $order_item) {

      $order_item_values = [
        'order_item_type_id' => $order_item->bundle(),
        'order_item_id' => $order_item->id(),
        'title' => $order_item->label(),
        'quantity' => $order_item->getQuantity(),
        'unit_price' => $order_item->getUnitPrice(),
        'total_price' => $order_item->getTotalPrice(),
        'adjusted_unit_price' => $order_item->getAdjustedUnitPrice(),
        'adjusted_total_price' => $order_item->getAdjustedTotalPrice(),
      ];

    }

    $this->createFromOrder($order, array_merge($order_item_values, $order_values, $shipping_values));

  }

  /**
   * {@inheritdoc}
   */
  public function buildQuery(QueryAggregateInterface $query) {
    $query->aggregate('title', 'COUNT');
    $query->aggregate('quantity', 'SUM');
    $query->aggregate('unit_price.number', 'SUM');
    $query->aggregate('total_price.number', 'SUM');
    $query->aggregate('adjusted_unit_price.number', 'SUM');
    $query->aggregate('adjusted_total_price.number', 'SUM');
    $query->groupBy('title');
    $query->groupBy('unit_price.currency_code');
  }

}
