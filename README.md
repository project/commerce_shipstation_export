# Commerce Shipstation Export

This is a simple module which extends the Drupal Commerce Reports module to allow us to get the shipping information out of the orders which can then be imported directly into Shipstation using: https://help.shipstation.com/hc/en-us/articles/360036323571-Import-Orders-via-CSV

## Installation
Since the module requires an external library, Composer or Ludwig must be used.

Once installed you can access the report at: admin/commerce/reports/shipstation-export

If there are no items in the report you may need to generate the report at: admin/commerce/config/reports/generate-reports
